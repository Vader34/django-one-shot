from django.shortcuts import render, redirect , get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def show_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists
    }
    return render(request, "todos/list.html",context)


def task_detail(request, id):
    todo_list = TodoList.objects.get(id=id)
    context = {
        "todo_list": todo_list
    }
    return render(request, "todos/details.html", context)


def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.id)
    else:
        form = TodoListForm()
            
    context = {
        "form": form,
        }
    return render(request,"todos/create.html", context)
    

def update_todolist(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
                form =form.save()
                return redirect("todo_list_detail", id=form.id)
    else:
        form = TodoListForm(instance=todo_list)
            
    context = {
        "form": form
        }

    return render(request,"todos/edit.html", context)


def delete_todolist(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.list.id)
    else:
        form = TodoItemForm()
            
    context = {
            "form": form,
        }
    return render(request,"todos/create_item.html", context)


def update_todo_item(request, id):
    update_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=update_item)
        if form.is_valid():
            update_item=form.save()
            return redirect("todo_list_detail", id=update_item.id)
        
    else:
        form = TodoItemForm(instance=update_item)

    context = {
        "form": form
    }
    return render(request, "todos/update_item.html", context)
