from django.urls import path
from todos.views import show_list, task_detail, create_todolist, update_todolist, delete_todolist, todo_item_create, update_todo_item

urlpatterns = [
    path("items/<int:id>/edit/", update_todo_item, name="todo_item_update"),
    path("items/create/",todo_item_create, name="todo_item_create"),
    path("<int:id>/delete/", delete_todolist, name="todo_list_delete"),
    path("<int:id>/edit/", update_todolist, name="todo_list_update"),
    path("create/", create_todolist, name="todo_list_create"),
    path("<int:id>/", task_detail, name="todo_list_detail"),
    path("", show_list, name="todo_list_list")
]
